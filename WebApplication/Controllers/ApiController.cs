﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models.Reports;

using System.Data.SqlClient;

using System.Web.Configuration;
using System.Collections;
using System.Data;

namespace WebApplication.Controllers
{

    public class ApiController : Controller
    {
        string strcon = WebConfigurationManager.ConnectionStrings["DATASERVER"].ConnectionString;
        // GET: Api
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListInventory()
        {

            Inventory objInventory = new Inventory();

            try
            {
                //abrir la conexion

                using (SqlConnection myConnection = new SqlConnection(strcon))
                {
                    myConnection.Open();

                    //Formar la sentencia SQL, un SELECT en este caso
                    SqlDataReader myReader = null;
                    string sql = "select t.Jobs,CAST(ROUND(t.TotalLinearYards,2) AS numeric(36,2)) AS Total from (select sum(linearyards) AS TotalLinearYards,COUNT(*) AS Jobs from salesorders as s inner join [SalesOrderJobs] as jobs on s.ID = jobs.[SalesOrderId] where DigitalDue>='2016-04-18 00:00:00.000') t";
                    SqlCommand myCommand = new SqlCommand(sql, myConnection);

                    //Ejecutar el comando SQL
                    myReader = myCommand.ExecuteReader();
                    //Mostrar los datos de la tabla
                    while (myReader.Read())
                    {
                        objInventory.Total = Convert.ToDecimal(myReader["Total"].ToString());
                        objInventory.Jobs = Convert.ToInt32(myReader["Jobs"].ToString());
                    }
                }



            }
            catch (Exception e)
            {
                throw;
            }
            
            var jsonResult = Json(objInventory, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            Response.StatusCode = 200;
            Response.ContentType = "application/json";
                
            return (jsonResult);
        }
        [HttpGet]
        public ActionResult ListOrdersJobs()
        {

            OrdersJobs objOJ;
            List<OrdersJobs> listOJ = new List<OrdersJobs>();
            try
            {
                //abrir la conexion

                using (SqlConnection myConnection = new SqlConnection(strcon))
                {
                    myConnection.Open();

                    //Formar la sentencia SQL, un SELECT en este caso
                    SqlDataReader myReader = null;
                    string sql = "SELECT t.DigitalDue,t.NumberOrders, CAST(ROUND(t.TotalLinearYards,2) AS numeric(36,2)) TotalLinearYards, CAST(ROUND(t.TotalLinearYardsMatching,2) AS numeric(36,2)) TotalLinearYardsMatching, (CAST(ROUND(((t.TotalLinearYardsMatching*100)/t.TotalLinearYards) ,2) AS numeric(36,2))) TotalPercent, t.Jobs FROM (select CONVERT(VARCHAR(10),DigitalDue,110) DigitalDue, Count(DISTINCT SalesORderNumber) NumberOrders, COUNT(s.Id) Jobs, SUM(LinearYards) TotalLinearYards, SUM(CASE WHEN Matching=1 THEN LinearYards ELSE 0 END) TotalLinearYardsMatching from salesorders as s inner join [SalesOrderJobs] as jobs on s.ID = jobs.[SalesOrderId] where DigitalDue>='2016-04-18 00:00:00.000' group by DigitalDue UNION ALL select 'Total',Count(DISTINCT SalesORderNumber),COUNT(s.Id), SUM(LinearYards),SUM(CASE WHEN Matching=1 THEN LinearYards ELSE 0 END) from salesorders as s inner join [SalesOrderJobs] as jobs on s.ID = jobs.[SalesOrderId] where DigitalDue>='2016-04-18 00:00:00.000') t";
                    SqlCommand myCommand = new SqlCommand(sql, myConnection);

                    //Ejecutar el comando SQL
                    myReader = myCommand.ExecuteReader();
                    //Mostrar los datos de la tabla
                    while (myReader.Read())
                    {
                        objOJ = new OrdersJobs();
                        objOJ.NumberOrders = Convert.ToInt32(myReader["NumberOrders"].ToString());
                        objOJ.Jobs = Convert.ToInt32(myReader["Jobs"].ToString());
                        objOJ.DigitalDue = myReader["DigitalDue"].ToString();
                        objOJ.TotalLinearYards = Convert.ToDecimal(myReader["TotalLinearYards"].ToString());
                        objOJ.TotalLinearYardsMatching = Convert.ToDecimal(myReader["TotalLinearYardsMatching"].ToString());
                        objOJ.TotalPercent = Convert.ToDecimal(myReader["TotalPercent"].ToString());
                        listOJ.Add(objOJ);
                    }
                }



            }
            catch (Exception e)
            {
                throw;
            }

            var jsonResult = Json(listOJ, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            Response.StatusCode = 200;
            Response.ContentType = "application/json";

            return (jsonResult);
        }

        [HttpGet]
        public ActionResult ListPriority()
        {

            Priority objPriority;
            List<Priority> listPriority = new List<Priority>();
            try
            {
                //abrir la conexion

                using (SqlConnection myConnection = new SqlConnection(strcon))
                {
                    

                    using (SqlCommand myCommand = new SqlCommand("Report3", myConnection))
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myConnection.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(myCommand);
                        DataSet ds = new DataSet();

                        adapter.Fill(ds);
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            objPriority = new Priority();
                            objPriority.NumberOrders = ds.Tables[0].Rows[i]["NumberOrders"].ToString();
                            objPriority.DigitalDue = ds.Tables[0].Rows[i]["DigitalDue"].ToString();
                            objPriority.TotalLinearYards = ds.Tables[0].Rows[i]["TotalLinearYards"].ToString();
                            objPriority.TotalLinearYardsMatching = ds.Tables[0].Rows[i]["TotalLinearYardsMatching"].ToString();
                            objPriority.TotalPercent = ds.Tables[0].Rows[i]["TotalPercent"].ToString();
                            objPriority.PriorityName = ds.Tables[0].Rows[i]["Priority"].ToString();
                            objPriority.Jobs = ds.Tables[0].Rows[i]["Jobs"].ToString();
                            listPriority.Add(objPriority);
                        }
                    }
                }



            }
            catch (Exception e)
            {
                throw;
            }

            var jsonResult = Json(listPriority, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            Response.StatusCode = 200;
            Response.ContentType = "application/json";

            return (jsonResult);
        }
    }
}