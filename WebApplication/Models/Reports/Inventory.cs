﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models.Reports
{
    public class Inventory
    {
        public decimal Total { get; set; }
        public int Jobs { get; set; }
    }
}