﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models.Reports
{
    public class Priority
    {
        public string PriorityName { get; set; }
        public string DigitalDue { get; set; }
        public string NumberOrders { get; set; }
        public string TotalLinearYards { get; set; }
        public string TotalLinearYardsMatching { get; set; }
        public string TotalPercent { get; set; }
        public string Jobs { get; set; }
    }
}