﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models.Reports
{
    public class OrdersJobs
    {
        public string DigitalDue { get; set; }
        public int NumberOrders { get; set; }
        public decimal TotalLinearYards { get; set; }
        public decimal TotalLinearYardsMatching { get; set; }
        public decimal TotalPercent { get; set; }
        public int Jobs { get; set; }
    }
}