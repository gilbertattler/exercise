﻿app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: "/Angular/tpl/home.html",
        controller: "inventoryControllers"
    }),

    $routeProvider.when('/priority', {
        templateUrl: "/Angular/tpl/priority.html",
        controller: "priorityControllers"
    }),

    $routeProvider.when('/ordersjobs', {
        
        templateUrl: "/Angular/tpl/ordersjobs.html",
        controller: "ordersJobsControllers"
    }),


    $routeProvider.otherwise({
        redirectTo: '/'
    });

}]);

$(document).ready(function () {
    onChangeSection($("#menu .navbar-nav > li > a[href='" + window.location.hash + "']"));
    onScrollScreen();
    onClickSectionLink();
});

function onChangeSection(link) {
    $("#menu .navbar-nav > li").removeClass("active");
    link.parent().addClass("active");
}

function onScrollScreen() {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $("#menu").addClass("opaque");
            return;
        }

        $("#menu").removeClass("opaque");
    });
}

function onClickSectionLink() {
    $("#menu .navbar-nav > li > a").click(function () {
        if (!$("#menuToggler").hasClass("collapsed")) {
            $("#menuToggler").click();
        }

        onChangeSection($(this));
    });
}

app.directive('toggle', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (attrs.toggle == "tooltip") {
                $(element).tooltip();
            }
            if (attrs.toggle == "popover") {
                $(element).popover();
            }
        }
    };
});


app.controller('ModalDemoCtrl', function ($scope, $modal, $log) {

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'privacity/privacity.html',
            controller: 'ModalInstanceCtrl',
            size: size
        });

        modalInstance.result.then(function (selectedItem) {
            //$scope.selected = selectedItem;
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };
});

app.controller('ModalInstanceCtrl', function ($scope, $modalInstance) {


    $scope.ok = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});