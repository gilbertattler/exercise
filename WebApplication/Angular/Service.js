﻿app.service("reportsService", function ($http, $q) {
    this.listInventory = function () {
        var q = $q.defer();

        var listInventory = [];

        var api = $http.get('http://localhost:6176/app/ListInventory');

        api.success(function (r) {
            listInventory = r;
            q.resolve(listInventory);
        });

        api.error(function (e) {
            q.reject(e);
        });

        api.finally(function () {

        });

        return q.promise;
    };
    this.listOrdersJobs = function () {
        var q = $q.defer();

        var listOrdersJobs = [];

        var api = $http.get('http://localhost:6176/app/ListOrdersJobs');

        api.success(function (r) {
            listOrdersJobs = r;
            q.resolve(listOrdersJobs);
        });

        api.error(function (e) {
            q.reject(e);
        });

        api.finally(function () {

        });

        return q.promise;
    };
    this.listPriority = function () {
        var q = $q.defer();

        var list = [];

        var api = $http.get('http://localhost:6176/app/ListPriority');

        api.success(function (r) {
            list = r;
            q.resolve(list);
        });

        api.error(function (e) {
            q.reject(e);
        });

        api.finally(function () {

        });

        return q.promise;
    };
});
