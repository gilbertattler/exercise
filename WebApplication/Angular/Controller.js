﻿
app.controller('inventoryControllers', function ($scope, $http, reportsService) {
    $scope.listInventory = [];

    reportsService.listInventory().then(function (r) {
        $scope.listInventory = r;
        $scope.chartJobs = {
            options: {
                chart: {
                    type: 'bar'
                },
                legend: {
                    enabled: false
                }
            },
            series: [{
                name: "Jobs",
                data: [$scope.listInventory.Jobs]
            }],
            title: {
                text: 'Jobs'
            }
        }
        $scope.chartTotal = {
            options: {
                chart: {
                    type: 'bar'
                },
                legend: {
                    enabled: false
                }
            },
            series: [{
                name: "Total Linear Yards",
                data: [$scope.listInventory.Total]
            }],
            title: {
                text: 'Total Linear Yards'
            }
        }
    }, function (e) {
        alert('Error en el servicio de listar el inventario');
    });



});
app.controller('ordersJobsControllers', function ($scope, $http, reportsService) {
    $scope.listOrdersJobs = [];
    $scope.jobsChart = [];
    reportsService.listOrdersJobs().then(function (r) {
        $scope.listOrdersJobs = r;
        angular.forEach($scope.listOrdersJobs, function (v, i) {
            if (v.DigitalDue != "Total") {
                $scope.jobsChart.push([v.DigitalDue, v.Jobs]);
            }
        });
        $scope.chartTotal = {
            options: {
                legend: {
                    enabled:false
                },
                chart: {
                    type: 'column',
                    borderRadius: 2,
                    borderWidth: 1,
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'rgb(255, 255, 255)'],
                            [1, 'rgb(200, 200, 255)']
                        ]
                    },
                    borderColor: "#CED8F6"
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'values'
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.y:.0f} Jobs</b>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                colors: ['#819FF7']
            },
            series: [{
                name: 'Jobs',
                data: $scope.jobsChart,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.0f}', 
                    y: 10, 
                    style: {
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                    }
                }
            }],
            title: {
                text: 'Jobs'
            }
        }
    }, function (e) {
        alert('Error en el servicio de listar los ordersjobs');
    });
});

app.controller('priorityControllers', function ($scope, $http, reportsService) {
    
    $scope.dateCategory = [];
    $scope.listPriority = [];
    $scope.listStandard = [];
    $scope.listRush = [];
    $scope.listFash = [];

    reportsService.listPriority().then(function (r) {
        $scope.listPriority = r;
        var isOk = false;
        var isStandard = false;
        var isRush = false;
        var isFash = false;
        var flagDigital = false;
        angular.forEach($scope.listPriority, function (v, i) {
            if(v.DigitalDue != "Total" && v.DigitalDue != ""){
                $scope.dateCategory.push(v.DigitalDue);
            }
        });
        angular.forEach($scope.dateCategory, function (vc, ic) {
            isStandard = false;
            isRush = false;
            isFash = false;
            angular.forEach($scope.listPriority, function (v, i) {
                if(v.DigitalDue!="" && v.DigitalDue!="Total"){
                    flagDigital = v.DigitalDue;
                }
                if(vc==flagDigital){
                    if(v.PriorityName == "Standard"){
                        $scope.listStandard.push(parseInt(v.Jobs,10));
                        isStandard = true;
                    }
                    if(v.PriorityName == "Rush"){
                        $scope.listRush.push(parseInt(v.Jobs,10));
                        isRush = true;
                    }
                    if(v.PriorityName == "Fast Track"){
                        $scope.listFash.push(parseInt(v.Jobs,10));
                        isFash = true;
                    }
                }
            });
            if(!isStandard){
                $scope.listStandard.push(0);
            }
            if(!isRush){
                $scope.listRush.push(0);
            }
            if(!isFash){
                $scope.listFash.push(0);
            }
        });
       $scope.chartTotal = {
            options: {
                legend: {
                    enabled:true
                },
                chart: {
                    type: 'column',
                    borderRadius: 2,
                    borderWidth: 1,
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'rgb(255, 255, 255)'],
                            [1, 'rgb(200, 200, 255)']
                        ]
                    },
                    borderColor: "#CED8F6"
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'values'
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.y:.0f} Jobs</b>'
                },
                xAxis: {
                    categories: $scope.dateCategory,
                    crosshair: true
                },
                colors: ['#058DC7', '#50B432', '#ED561B']
            },
            series: [
                        {name:"Standard", data:$scope.listStandard},
                        {name:"Rush", data:$scope.listRush},
                        {name:"Fast Track", data:$scope.listFash}
                    ],
            title: {
                text: 'Jobs'
            }
        }
    }, function (e) {
        alert('Error en el servicio de listar los ordersjobs');
    });
});